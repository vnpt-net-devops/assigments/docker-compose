## products microservice with MySQL

$ cd products
$ sudo apt install npm
$ npm i
$ docker-compose up


## orders microservice with MongoDB

$ cd orders
$ npm i
$ docker-compose up


## API Gateway Pattern Architecture

$ cd api-gateway
## change in URLs.js file 192.168.1.10 with your local IP
## using ifconfig to get it
$ npm i
$ docker-compose up


## Using API Gateway on browser


http://<your-local-IP>:3007/orders
http://<your-local-IP>:3007/products

